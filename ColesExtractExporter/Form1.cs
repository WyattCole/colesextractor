﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ColesExtractExporter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<OutputRow> rows = getRows();

            FileHelpers.DelimitedFileEngine<OutputRow> csv = new FileHelpers.DelimitedFileEngine<OutputRow>();
            csv.HeaderText = csv.GetFileHeader();
            csv.WriteFile("C:\\Temp\\ColesExtract.CSV", rows);

            MessageBox.Show("Yeah, done");
        }

        private List<OutputRow> getRows()
        {
            List<OutputRow> result = new List<OutputRow>();

            using (SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=Utopia_Coles;Integrated Security=True"))
            {
                connection.Open();

                try
                {
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.CommandType = CommandType.Text;
                    command.CommandTimeout = 0;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(readRow(reader));
                        }
                    }
                }

                catch (Exception ex)
                {
                    { }
                }
            }

            return result;
        }

        private OutputRow readRow(SqlDataReader reader)
        {
            return new OutputRow
            {
                ColesSupplierCode = (reader["ColesSupplierCode"] as string)?.Trim(),
                WooliesSupplierCode = (reader["WooliesSupplierCode"] as string)?.Trim(),
                Date = reader["Date"] as DateTime?,
                Location = (reader["Location"] as string)?.Trim(),
                Category = (reader["Category"] as string)?.Trim(),
                BCM = (reader["BCM"] as string)?.Trim(),
                BU = (reader["BU"] as string)?.Trim(),
                Barcode = (reader["EAN"] as string)?.Trim(),
                Brand = (reader["Brand"] as string)?.Trim(),
                Name = (reader["Name"] as string)?.Trim(),
                Quantity = reader["Quantity"] as int?,
                QuantityDefinition = (reader["QuantityDefinition"] as string)?.Trim(),
                Weight = reader["Weight"] as decimal?,
                WeightDefinition = (reader["WeightDefinition"] as string)?.Trim(),
                PrivateLabel = reader["PrivateLabel"] as int?,
                AvailableColes = reader["AvailableColes"] as int?,
                AvailableWoolies = reader["AvailableWoolies"] as int?,
                CreatedColes = reader["ColesCreated"] as DateTime?,
                CreatedWoolies = reader["WooliesCreated"] as DateTime?,
            };
        }

        private string sql = @"
DECLARE @StartDate DATE;
DECLARE @EndDate DATE;

SET @StartDate = '2017-12-01'
SET @EndDate = GETDATE()

SELECT			SPRSPH.SupplierPricingRegionSupplierProductId,
				SPRSP.SupplierPricingRegionId,
				SPRSP.SupplierProductId,
				[Dates].[Date],
				SPRSPH.StartDate,
				SPRSPH.EndDate,
				SPRSPH.Price,
				SPRSPH.Discount

INTO			#tmp

FROM (			SELECT			[Date]

				FROM (			SELECT			DATEADD(DAY, Number, @StartDate) AS [Date]

								FROM (			SELECT			DISTINCT
																Number 
												FROM			master.dbo.spt_values
												WHERE			Name IS NULL) AS n

								WHERE			DATEADD(DAY, Number, @StartDate) < @EndDate) AS Dates

				WHERE			DATEPART(DAY, [Date]) = 15) AS Dates

INNER JOIN (	SELECT			*
				FROM			SupplierPricingRegionSupplierProduct) AS SPRSP
	ON			1 = 1

LEFT JOIN (		SELECT			*
				FROM			SupplierPricingRegionSupplierProductHistory) AS SPRSPH
	ON			SPRSP.Id = SPRSPH.SupplierPricingRegionSupplierProductId
		AND		Dates.[Date] BETWEEN SPRSPH.StartDate AND ISNULL(SPRSPH.EndDate, GETDATE())

WHERE			SPRSPH.SupplierPricingRegionSupplierProductId IS NOT NULL

CREATE INDEX IX_SupplierPricingRegionSupplierProductId ON #tmp(SupplierPricingRegionSupplierProductId);
CREATE INDEX IX_Date ON #tmp([Date]);

SELECT			ColesSupplierCode, 
				WooliesSupplierCode,
				Date,
				Location,
				Category,
				BCM,
				BU,
				EAN,
				Brand,
				Name,
				Quantity,
				QuantityDefinition,
				Weight,
				WeightDefinition,
				PrivateLabel,
				AvailableColes,
				AvailableWoolies,
				ColesCreated,
				WooliesCreated

FROM (			SELECT			ROW_NUMBER() OVER (PARTITION BY ISNULL(ColesSupplierCode, WooliesSupplierCode), Date, Location ORDER BY (AvailableColes + AvailableWoolies) DESC) AS RN,
								*

				FROM (			SELECT			DISTINCT
												Coles.SupplierCode AS ColesSupplierCode, 
												Woolies.SupplierCode AS WooliesSupplierCode,
												ISNULL(Coles.Date, Woolies.Date) AS Date,
												ISNULL(Coles.Location, Woolies.Location) AS Location,
												ISNULL(Coles.Category, Woolies.Category) AS Category,
												ISNULL(Coles.BCM, Woolies.BCM) AS BCM,
												ISNULL(Coles.BU, Woolies.BU) AS BU,
												ISNULL(Coles.EAN, Woolies.EAN) AS EAN,
												ISNULL(Coles.Brand, Woolies.Brand) AS Brand,
												ISNULL(Coles.Name, Woolies.Name) AS Name,
												ISNULL(Coles.Quantity, Woolies.Quantity) AS Quantity,
												ISNULL(Coles.QuantityDefinition, Woolies.QuantityDefinition) AS QuantityDefinition,
												ISNULL(Coles.Weight, Woolies.Weight) AS Weight,
												ISNULL(Coles.WeightDefinition, Woolies.WeightDefinition) AS WeightDefinition,
												CASE 
												WHEN Coles.PrivateLabel = 1 THEN 1
												WHEN Woolies.PrivateLabel = 1 THEN 1
												ELSE 0
												END AS PrivateLabel,
												CASE 
												WHEN Coles.Date IS NOT NULL THEN 1 
												ELSE 0
												END AS AvailableColes,
												CASE
												WHEN Woolies.Date IS NOT NULL THEN 1
												ELSE 0
												END AS AvailableWoolies,
												Coles.CreatedAt AS ColesCreated,
												Woolies.CreatedAt AS WooliesCreated

								FROM (			SELECT			SP.Id AS SupplierProductId,
																CPD.PartNumber AS SupplierCode,
																SPRSP.[Date],
																SPR.Code AS [Location],
																CPD.CategoryName AS Category,
																CPD.BCM,
																CPD.BU,
																CPD.Barcode AS EAN,
																P.Brand,
																P.Name,
																P.Quantity,
																P.QuantityDefinition,
																P.Weight,
																P.WeightDefinition,

																CASE WHEN P.Brand IN (
																	'Balnea',
																	'Banquet',
																	'Baxter''s',
																	'By George',
																	'Carbonell',
																	'Chevron',
																	'Classic Essentials',
																	'Coles',
																	'Coles Bakery',
																	'Coles Dairy',
																	'Coles Deli',
																	'Coles Deli Express',
																	'Coles Finest',
																	'Coles Fresh',
																	'Coles Grill',
																	'Coles Made Easy',
																	'Coles Mobile',
																	'Coles Online',
																	'Coles Party',
																	'Coles Patisserie',
																	'Coles Simply Gluten Free',
																	'Coles Simply Less',
																	'Coles Smart Buy',
																	'Comfy Bots',
																	'Cook & Dine',
																	'Drovers Choice',
																	'Essentials',
																	'First Force',
																	'Gold',
																	'Heston For Coles',
																	'Hillview',
																	'Home Essentials',
																	'Homebrand',
																	'Inspire',
																	'Jackson',
																	'Jamie Oliver',
																	'Little One''s',
																	'Olsent',
																	'Smitten',
																	'Softly',
																	'Strike',
																	'Woolworth',
																	'Woolworths',
																	'Woolworths Essentials',
																	'Woolworths For Her',
																	'Woolworths For Him',
																	'Woolworths For Kids',
																	'Woolworths For Ladies',
																	'Woolworths For Men',
																	'Woolworths Gold',
																	'Woolworths Select',
																	'Wow',
																	'Coles',
																	'Smart Buy',
																	'Finest',
																	'Comfy Bots',
																	'Purr',
																	'Cook & Dine',
																	'Drovers choice',
																	'Susan Day',
																	'La Espanola',
																	'Seacrown',
																	'Laurent',
																	'Complete cuisine',
																	'Banquet Dog',
																	'Gold choice',
																	'Gold coin',
																	'Le fleur',
																	'Alchemy',
																	'Startlet',
																	'Rosslyn Bay',
																	'Graze Grassfed',
																	'Cleavers organic',
																	'Miller & Moore',
																	'The Bakers Tray',
																	'Cakes west',
																	'Margaret River organic',
																	'Classic',
																	'Joy',
																	'Meat Servery',
																	'Stella & Kate')
																THEN 1
																ELSE 0
																END AS [PrivateLabel],
																SP.CreatedAt

												FROM (			SELECT			*
																FROM			Supplier
																WHERE			Code = 'COLES') AS S

												INNER JOIN (	SELECT			*
																FROM			SupplierProduct
																WHERE			Suppressed = 0) AS SP
													ON			SP.SupplierId = S.Id

												--INNER JOIN (	SELECT			SP.*,
												--								SPE.EAN AS EAN2
												--				FROM			SupplierProduct AS SP
												--				LEFT JOIN		SupplierProductEAN AS SPE
												--					ON			SPE.SupplierProductId = SP.Id
												--				WHERE			Suppressed = 0) AS SP
												--	ON			SP.SupplierId = S.Id

												INNER JOIN (	SELECT			*
																FROM			Product
																WHERE			Suppressed = 0) AS P
													ON			P.Id = SP.ProductId

												INNER JOIN (	SELECT			*
																FROM			SupplierPricingRegion) AS SPR
													ON			SPR.SupplierId = S.Id

												--INNER JOIN (	SELECT			*
												--				FROM			SupplierPricingRegionSupplierProduct
												--				WHERE			Expired = 0
												--					AND			Suppressed = 0) AS SPRSP
												--	ON			SPRSP.SupplierPricingRegionId = SPR.Id
												--		AND		SPRSP.SupplierProductId = SP.Id

												INNER JOIN (	SELECT			*
																FROM			#tmp) AS SPRSP
													ON			SPRSP.SupplierPricingRegionId = SPR.Id
														AND		SPRSP.SupplierProductId = SP.Id

												INNER JOIN (	SELECT			DISTINCT
																				Code,
																				PartNumber
																FROM			ColesData..ColesCodeMap_20180301) AS CCM
													ON			SP.Code COLLATE Latin1_General_CI_AS = CCM.Code COLLATE Latin1_General_CI_AS

												INNER JOIN (	SELECT			DISTINCT
																				A.PartNumber,
																				CAST(CAST(A.Barcode AS BIGINT) AS NCHAR(32)) AS Barcode,
																				B.CategoryName,
																				B.BCM,
																				B.BU
																FROM			ColesData..ColesProduct_20180301 AS A
																
																INNER JOIN		ColesData..ColesCategory_20180409 AS B
																	ON			A.Catagory = B.CategoryId) AS CPD
													ON			CPD.PartNumber = CCM.PartNumber

												--INNER JOIN (	SELECT			DISTINCT
												--								Category,
												--								SubCategory,
												--								CategoryName,
												--								SubcategoryName
												--				FROM			ColesCategory
												--				WHERE			CategoryName IN ('CEREAL', 'CHILLED DESSERTS', 'COFFEE')) AS CC
												--	ON			CC.Category = CBD.Category
												--		AND		CC.SubCategory = CBD.SubCategory
														) AS Coles

								FULL OUTER JOIN (SELECT			SP.Id AS SupplierProductId,
																SP.Code AS SupplierCode,
																SPRSP.[Date],
																SPR.Code AS [Location],
																WPCC.CategoryName AS Category,
																WPCC.BCM,
																WPCC.BU,
																WPCC.Barcode AS EAN,
																P.Brand,
																P.Name,
																P.Quantity,
																P.QuantityDefinition,
																P.Weight,
																P.WeightDefinition,

																CASE WHEN P.Brand IN (
																	'Balnea',
																	'Banquet',
																	'Baxter''s',
																	'By George',
																	'Carbonell',
																	'Chevron',
																	'Classic Essentials',
																	'Coles',
																	'Coles Bakery',
																	'Coles Dairy',
																	'Coles Deli',
																	'Coles Deli Express',
																	'Coles Finest',
																	'Coles Fresh',
																	'Coles Grill',
																	'Coles Made Easy',
																	'Coles Mobile',
																	'Coles Online',
																	'Coles Party',
																	'Coles Patisserie',
																	'Coles Simply Gluten Free',
																	'Coles Simply Less',
																	'Coles Smart Buy',
																	'Comfy Bots',
																	'Cook & Dine',
																	'Drovers Choice',
																	'Essentials',
																	'First Force',
																	'Gold',
																	'Heston For Coles',
																	'Hillview',
																	'Home Essentials',
																	'Homebrand',
																	'Inspire',
																	'Jackson',
																	'Jamie Oliver',
																	'Little One''s',
																	'Olsent',
																	'Smitten',
																	'Softly',
																	'Strike',
																	'Woolworth',
																	'Woolworths',
																	'Woolworths Essentials',
																	'Woolworths For Her',
																	'Woolworths For Him',
																	'Woolworths For Kids',
																	'Woolworths For Ladies',
																	'Woolworths For Men',
																	'Woolworths Gold',
																	'Woolworths Select',
																	'Wow',
																	'Coles',
																	'Smart Buy',
																	'Finest',
																	'Comfy Bots',
																	'Purr',
																	'Cook & Dine',
																	'Drovers choice',
																	'Susan Day',
																	'La Espanola',
																	'Seacrown',
																	'Laurent',
																	'Complete cuisine',
																	'Banquet Dog',
																	'Gold choice',
																	'Gold coin',
																	'Le fleur',
																	'Alchemy',
																	'Startlet',
																	'Rosslyn Bay',
																	'Graze Grassfed',
																	'Cleavers organic',
																	'Miller & Moore',
																	'The Bakers Tray',
																	'Cakes west',
																	'Margaret River organic',
																	'Classic',
																	'Joy',
																	'Meat Servery',
																	'Stella & Kate')
																THEN 1
																ELSE 0
																END AS [PrivateLabel],
																SP.CreatedAt

												FROM (			SELECT			*
																FROM			Supplier
																WHERE			Code = 'WOOLIES') AS S

												INNER JOIN (	SELECT			*
																FROM			SupplierProduct
																WHERE			Suppressed = 0) AS SP
													ON			SP.SupplierId = S.Id

												INNER JOIN (	SELECT			*
																FROM			Product
																WHERE			Suppressed = 0) AS P
													ON			P.Id = SP.ProductId

												INNER JOIN (	SELECT			*
																FROM			SupplierPricingRegion) AS SPR
													ON			SPR.SupplierId = S.Id

												INNER JOIN (	SELECT			*
																FROM			#tmp) AS SPRSP
													ON			SPRSP.SupplierPricingRegionId = SPR.Id
														AND		SPRSP.SupplierProductId = SP.Id

												INNER JOIN (	SELECT			DISTINCT
																				A.SupplierProductCode,
																				A.Barcode,
																				A.CategoryId,
																				B.CategoryName,
																				B.BCM,
																				B.BU
																FROM			ColesData..WooliesProductColesCategory_20180301 AS A
																INNER JOIN		ColesData..ColesCategory_20180409 AS B
																	ON			A.CategoryId = B.CategoryId) AS WPCC
													ON			WPCC.SupplierProductCode COLLATE Latin1_General_CI_AS = SP.Code COLLATE Latin1_General_CI_AS) AS Woolies
									ON			Coles.Location COLLATE Latin1_General_CI_AS = Woolies.Location COLLATE Latin1_General_CI_AS
										AND		Coles.EAN COLLATE Latin1_General_CI_AS = Woolies.EAN COLLATE Latin1_General_CI_AS

								WHERE			ISNULL(Coles.Category, Woolies.Category) IS NOT NULL) AS X
				) AS X

WHERE			RN = 1

DROP TABLE #tmp;
";
    }
}
