﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace ColesExtractExporter
{
    [DelimitedRecord(",")]
    public class OutputRow
    {
        [FieldOrder(1), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string ColesSupplierCode;

        [FieldOrder(2), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string WooliesSupplierCode;

        [FieldOrder(3), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth), FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime? Date;

        [FieldOrder(4), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string Location;

        [FieldOrder(5), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string Category;

        [FieldOrder(6), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string BCM;

        [FieldOrder(7), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string BU;

        [FieldOrder(8), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string Barcode;

        [FieldOrder(9), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string Brand;

        [FieldOrder(10), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string Name;

        [FieldOrder(11), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public int? Quantity;

        [FieldOrder(12), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string QuantityDefinition;

        [FieldOrder(13), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public decimal? Weight;

        [FieldOrder(14), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public string WeightDefinition;

        [FieldOrder(15), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public int? PrivateLabel;

        [FieldOrder(16), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public int? AvailableColes;

        [FieldOrder(17), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth)]
        public int? AvailableWoolies;

        [FieldOrder(18), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth), FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime? CreatedColes;

        [FieldOrder(19), FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.AllowForBoth), FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime? CreatedWoolies;
    }
}
